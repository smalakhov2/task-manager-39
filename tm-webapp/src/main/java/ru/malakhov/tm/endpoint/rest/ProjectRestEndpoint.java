package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.IProjectRestEndpoint;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @PostMapping
    public void create(@RequestBody String name) {
        projectService.create("123", name);
    }

    @Override
    @PutMapping
    public void update(@RequestBody ProjectDTO projectDTO) {
        projectService.createDTO("123", projectDTO);
    }

    @Override
    @GetMapping("/${id}")
    public ProjectDTO findOneByIdDTO(@PathVariable("id") String id) {
        return projectService.findOneByIdDTO("123", id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return projectService.existsById(id);
    }

    @Override
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) {
        projectService.removeOneById("123", id);
    }

}