package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.ITaskRestEndpoint;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;

@RestController
@RequestMapping("/api/task")
public class TaskRestEndpoint implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @PostMapping
    public void create(@RequestBody String name) {
        taskService.create("123", name);
    }

    @Override
    @PutMapping
    public void update(@RequestBody TaskDTO task) {
        taskService.createDTO("123", task);
    }

    @Override
    @GetMapping("/${id}")
    public TaskDTO findOneByIdDTO(@PathVariable("id") String id) {
        return taskService.findOneByIdDTO("123", id);
    }

    @Override
    @GetMapping("/exist/${id}")
    public boolean existsById(@PathVariable("id") String id) {
        return taskService.existsById(id);
    }

    @Override
    @DeleteMapping("/${id}")
    public void deleteOneById(@PathVariable("id") String id) {
        taskService.removeOneById("123", id);
    }

}