package ru.malakhov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskSoapEndpoint {

    @Autowired
    private ITaskService taskService;

    @WebMethod
    public List<TaskDTO> getTasksListDTO() {
        return taskService.findAllDto();
    }

    @WebMethod
    public void deleteAllTasks() {
        taskService.removeAll("123");
    }

    @WebMethod
    public void createTask(@WebParam(name = "name") String name) {
        taskService.create("123", name);
    }

    @WebMethod
    public void updateTask(@WebParam(name = "task") TaskDTO task) {
        taskService.createDTO("123", task);
    }

    @WebMethod
    public TaskDTO findOneTaskByIdDTO(@WebParam(name = "id") String id) {
        return taskService.findOneByIdDTO("123", id);
    }

    @WebMethod
    public boolean existsTaskById(@WebParam(name = "id") String id) {
        return taskService.existsById(id);
    }

    @WebMethod
    public void deleteOneTaskById(@WebParam(name = "id") String id) {
        taskService.removeOneById("123", id);
    }

}