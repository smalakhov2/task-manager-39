package ru.malakhov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.api.endpoint.ITasksRestEndpoint;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping
    public List<TaskDTO> getListDTO() {
        return taskService.findAllDto();
    }

    @Override
    @PostMapping
    public List<Task> saveAll(@RequestBody List<Task> list) {
        return taskService.saveAll(list);
    }

    @Override
    @DeleteMapping("/")
    public void deleteAll(@RequestBody List<Task> list) {
        taskService.deleteAll(list);
    }

    @Override
    @DeleteMapping("/all")
    public void deleteAll() {
        taskService.removeAll("123");
    }

}