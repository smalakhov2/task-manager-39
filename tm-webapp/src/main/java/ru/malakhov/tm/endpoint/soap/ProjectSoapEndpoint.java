package ru.malakhov.tm.endpoint.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectSoapEndpoint {

    @Autowired
    private IProjectService projectService;

    @WebMethod
    public List<ProjectDTO> getListDTO() {
        return projectService.findAllDto();
    }

    @WebMethod
    public void deleteAll() {
        projectService.removeAll("123");
    }

    @WebMethod
    public void create(@WebParam(name = "name") String name) {
        projectService.create("123", name);
    }

    @WebMethod
    public void update(@WebParam(name = "project") ProjectDTO projectDTO) {
        projectService.createDTO("123", projectDTO);
    }

    @WebMethod
    public ProjectDTO findOneByIdDTO(@WebParam(name = "id") String id) {
        return projectService.findOneByIdDTO("123", id);
    }

    @WebMethod
    public boolean existsById(@WebParam(name = "id") String id) {
        return projectService.existsById(id);
    }

    @WebMethod
    public void deleteOneById(@WebParam(name = "id") String id) {
        projectService.removeOneById("123", id);
    }

}