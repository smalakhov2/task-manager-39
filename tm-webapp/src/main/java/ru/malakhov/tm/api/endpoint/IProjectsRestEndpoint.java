package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectsRestEndpoint {

    @GetMapping
    List<ProjectDTO> getListDTO();

    @PostMapping
    List<Project> saveAll(@RequestBody List<Project> list);

    @DeleteMapping("/")
    void deleteAll(@RequestBody List<Project> list);

    @DeleteMapping("/all")
    void deleteAll();

}