package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;

import java.util.List;

@RequestMapping("/api/tasks")
public interface ITasksRestEndpoint {

    @GetMapping
    List<TaskDTO> getListDTO();

    @PostMapping
    List<Task> saveAll(@RequestBody List<Task> list);

    @DeleteMapping("/")
    void deleteAll(@RequestBody List<Task> list);

    @DeleteMapping("/all")
    void deleteAll();

}