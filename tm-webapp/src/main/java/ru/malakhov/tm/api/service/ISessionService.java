package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.SessionDTO;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    @Nullable
    String getUserId(@Nullable SessionDTO session);

    @Nullable
    List<SessionDTO> getSessionList(@Nullable SessionDTO session);

    @NotNull
    List<SessionDTO> findAll();

    void close(@Nullable SessionDTO session);

    void closeAll(@Nullable SessionDTO session);

    boolean isValid(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session);

    void validate(@Nullable SessionDTO session, @Nullable Role role);

}