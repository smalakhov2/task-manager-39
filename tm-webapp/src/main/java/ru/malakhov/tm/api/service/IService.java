package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    void deleteAll();

    void load(@Nullable Collection<E> e);

    <S extends E> List<S> saveAll(Iterable<S> iterable);

    public void deleteAll(Iterable<? extends E> iterable);

    boolean existsById(@Nullable String id);

}