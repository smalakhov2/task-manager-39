package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(@Nullable String userId, @Nullable String name);

    void createDTO(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable Project project);

    void createDTO(@Nullable String userId, @Nullable ProjectDTO project);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<ProjectDTO> findAllDto();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @Nullable
    Project findById(final @Nullable String id);

    @NotNull
    Project findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    ProjectDTO findOneByIdDTO(@Nullable String userId, @Nullable String id);

    @NotNull
    Project findOneByName(@Nullable String userId, @Nullable String name);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void updateProjectById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void updateProjectByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}