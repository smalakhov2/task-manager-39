package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.TaskDTO;

@RequestMapping("/api/task")
public interface ITaskRestEndpoint {

    @PostMapping
    void create(@RequestBody String name);

    @PutMapping
    void update(@RequestBody TaskDTO task);

    @GetMapping("/${id}")
    TaskDTO findOneByIdDTO(@PathVariable("id") String id);

    @GetMapping("/exist/${id}")
    boolean existsById(@PathVariable("id") String id);

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id);

}