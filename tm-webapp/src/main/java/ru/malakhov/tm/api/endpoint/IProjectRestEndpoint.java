package ru.malakhov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.ProjectDTO;

@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    @PostMapping
    void create(@RequestBody String name);

    @PutMapping
    void update(@RequestBody ProjectDTO projectDTO);

    @GetMapping("/${id}")
    ProjectDTO findOneByIdDTO(@PathVariable("id") String id);

    @GetMapping("/exist/${id}")
    boolean existsById(@PathVariable("id") String id);

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id);

}