package ru.malakhov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void createDTO(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable Task task);

    void createDTO(@Nullable String userId, @Nullable TaskDTO taskDTO);

    void create(@Nullable String userId, @Nullable String projectName, @Nullable String name);

    void create(@Nullable String userId, @Nullable String projectName, @Nullable String name, @Nullable String description);

    @NotNull
    List<Task> findAll();

    void removeAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAllDto();

    @NotNull
    List<Task> findAllByUserIdAndProjectName(@Nullable String userId, @Nullable String projectName);
    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

    List<TaskDTO> findAllByProjectId(@Nullable String projectId);

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    TaskDTO findOneByIdDTO(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @Nullable String projectName, @Nullable Integer index);

    @NotNull
    Task findOneByName(@Nullable String userId, @Nullable String name);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable String projectName,  @Nullable Integer index);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void removeAllByProjectName(@Nullable String userId, @Nullable String projectName);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void updateTaskById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByName(@Nullable String userId, @Nullable String oldName, @Nullable String newName, @Nullable String description);

    void updateTaskByIndex(@Nullable String userId, @Nullable String projectName, @Nullable Integer index, @Nullable String name, @Nullable String description);

}