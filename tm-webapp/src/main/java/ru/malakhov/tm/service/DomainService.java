package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.dto.Domain;

@Service
public final class DomainService implements IDomainService {

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @Override
    public void load(final @Nullable Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTaskList());
        projectService.load(domain.getProjectList());
        userService.load(domain.getUserList());
    }

    @Override
    public void export(final @Nullable Domain domain) {
        if (domain == null) return;
        domain.setProjectList(projectService.findAll());
        domain.setTaskList(taskService.findAll());
        domain.setUserList(userService.findAll());
    }

}