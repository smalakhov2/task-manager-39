package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.exception.empty.EmptyDescriptionException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.project.ProjectNotFoundException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.exception.task.TaskNotFoundException;
import ru.malakhov.tm.repository.dto.ITaskRepositoryDTO;
import ru.malakhov.tm.repository.entity.ITaskRepository;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.dto.TaskDTO;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private ITaskRepositoryDTO taskRepositoryDTO;


    @Override
    public void create(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Task task = new Task();
        task.setName(name);
        task.setUser(userService.findById(userId));
        taskRepository.save(task);
    }

    @Override
    public void createDTO(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        taskRepositoryDTO.save(task);
    }

    @Override
    public void create(@Nullable String userId, @Nullable Task task) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new TaskNotFoundException();
        task.setUser(userService.findById(userId));
        taskRepository.save(task);
    }

    @Override
    public void createDTO(@Nullable String userId, @Nullable TaskDTO taskDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (taskDTO == null) throw new TaskNotFoundException();
        taskDTO.setUserId("123");
        taskRepositoryDTO.save(taskDTO);
    }

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String name, @Nullable String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        final @NotNull Project project = projectService.findOneByName(userId, projectName);
        final @NotNull Task task = new Task();
        task.setName(name);
        task.setUser(userService.findById(userId));
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String name, @Nullable String projectName, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        final @NotNull Project project = projectService.findOneByName(userId, projectName);
        final @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.findById(userId));
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    public @NotNull List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void removeAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Override
    public @NotNull List<TaskDTO> findAllDto() {
        return taskRepositoryDTO.findAll();
    }

    @Override
    public @NotNull List<Task> findAllByUserIdAndProjectName(final @Nullable String userId, @Nullable String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByUserIdAndProjectName(userId, projectName);
    }

    @Override
    public @NotNull List<TaskDTO> findAllByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepositoryDTO.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        return taskRepositoryDTO.findAllByProjectId(projectId);
    }

    @Override
    public @NotNull Task findOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @Nullable Task task = taskRepository.findOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public @NotNull TaskDTO findOneByIdDTO(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @Nullable TaskDTO taskDTO = taskRepositoryDTO.findOneByUserIdAndId(userId, id);
        if (taskDTO == null) throw new TaskNotFoundException();
        return taskDTO;
    }

    @Override
    public @NotNull Task findOneByIndex(final @Nullable String userId, final @Nullable String projectName, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        final @NotNull List<Task> taskList = taskRepository.findAllByUserIdAndProjectName(userId, projectName);
        return taskList.get(index);
    }

    @Override
    public @NotNull Task findOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Task task = taskRepository.findOneByUserIdAndName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    @Transactional
    public void removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        taskRepository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(final @Nullable String userId, @Nullable String projectName, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        final @NotNull Task task = findOneByIndex(userId, projectName, index);
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        taskRepository.deleteOneByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeAllByProjectName(@Nullable String userId, @Nullable String projectName) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.deleteAllByUserIdAndProjectName(userId, projectName);
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@Nullable String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.deleteAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void updateTaskById(final @Nullable String userId, final @Nullable String id,
                               final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void updateTaskByName(@Nullable String userId, @Nullable String oldName, @Nullable String newName, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new EmptyIdException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Task task = findOneByName(userId, oldName);
        task.setName(newName);
        task.setDescription(description);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void updateTaskByIndex(final @Nullable String userId, @Nullable String projectName, final @Nullable Integer index,
                                  final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectName == null || projectName.isEmpty()) throw new ProjectNotFoundException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Task task = findOneByIndex(userId, projectName, index);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
    }

}