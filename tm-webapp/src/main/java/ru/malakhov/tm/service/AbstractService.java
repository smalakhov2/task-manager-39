package ru.malakhov.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IService;
import ru.malakhov.tm.entity.AbstractEntity;
import ru.malakhov.tm.repository.entity.IRepository;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    private IRepository repository;

    @Override
    @Transactional
    public void load(@Nullable Collection<E> e) {
        if (e == null) return;
        repository.saveAll(e);
    }

    @Override
    @Transactional
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public <S extends E> List<S> saveAll(Iterable<S> iterable) {
        return repository.saveAll(iterable);
    }

    @Override
    public void deleteAll(Iterable<? extends E> iterable) {
        repository.deleteAll(iterable);
    }

    @Override
    public boolean existsById(@Nullable String id) {
        return repository.existsById(id);
    }

}