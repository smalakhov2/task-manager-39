package ru.malakhov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.malakhov.tm.api.service.IDataService;
import ru.malakhov.tm.api.service.IDomainService;
import ru.malakhov.tm.constant.DataConstant;
import ru.malakhov.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public final class DataService implements IDataService {

    @NotNull
    private IDomainService domainService;

    @Override
    public void binaryClear() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void binaryLoad() throws Exception {
        try(final @NotNull FileInputStream fileInputStream = new FileInputStream(DataConstant.DATA_BINARY_PATH)){
            try(final @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {
                final @NotNull Domain domain = (Domain) objectInputStream.readObject();
                domainService.load(domain);
            }
        }
    }

    @Override
    public void binarySave() throws Exception {
        final @NotNull Domain domain = new Domain();
        domainService.export(domain);
        final @NotNull File file = new File(DataConstant.DATA_BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        try(final @NotNull FileOutputStream fileOutputStream = new FileOutputStream(file)) {
            try(final @NotNull ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
                objectOutputStream.writeObject(domain);
            }
        }
    }

    @Override
    public void jsonClearFasterxml() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_FASTERXML_JSON_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void jsonLoadFasterxml() throws Exception {
        final @NotNull String json = new String(Files.readAllBytes(Paths.get(DataConstant.DATA_FASTERXML_JSON_PATH)));

        final @NotNull ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
        final @NotNull Domain domain = objectMapper.readValue(json, Domain.class);

        domainService.load(domain);
    }

    @Override
    public void jsonSaveFasterxml() throws Exception {
        final @NotNull Domain domain = new Domain();
        domainService.export(domain);

        final @NotNull File file = new File(DataConstant.DATA_FASTERXML_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final @NotNull ObjectMapper objectMapper = new ObjectMapper();
        final @NotNull String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try(final @NotNull FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.DATA_FASTERXML_JSON_PATH)) {
            fileOutputStream.write(json.getBytes());
            fileOutputStream.flush();
        }
    }

    @Override
    public void xmlClearFasterxml() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_FASTERXML_XML_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void xmlLoadFasterxml() throws Exception {
        final String xml = new String(Files.readAllBytes(Paths.get(DataConstant.DATA_FASTERXML_XML_PATH)));

        final @NotNull XmlMapper xmlMapper = new XmlMapper();
        final @NotNull Domain domain = xmlMapper.readValue(xml, Domain.class);

        domainService.load(domain);
    }

    @Override
    public void xmlSaveFasterxml() throws Exception {
        final @NotNull Domain domain = new Domain();
        domainService.export(domain);

        final @NotNull File file = new File(DataConstant.DATA_FASTERXML_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final @NotNull ObjectMapper objectMapper = new XmlMapper();
        final @NotNull String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        try(final @NotNull FileOutputStream fileOutputStream = new FileOutputStream(DataConstant.DATA_FASTERXML_XML_PATH)){
            fileOutputStream.write(xml.getBytes());
            fileOutputStream.flush();
        }
    }

    @Override
    public void jsonClearJaxb() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_JAXB_JSON_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void jsonLoadJaxb() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_JAXB_JSON_PATH);

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final @NotNull Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        final @NotNull Domain domain = (Domain) unmarshaller.unmarshal(file);

        domainService.load(domain);
    }

    @Override
    public void jsonSaveJaxb() throws Exception {
        final @NotNull Domain domain = new Domain();
        domainService.export(domain);

        final @NotNull File file = new File(DataConstant.DATA_JAXB_JSON_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final @NotNull Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, file);
    }

    @Override
    public void xmlClearJaxb() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_JAXB_XML_PATH);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void xmlLoadJaxb() throws Exception {
        final @NotNull File file = new File(DataConstant.DATA_JAXB_XML_PATH);

        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final @NotNull Domain domain = (Domain) jaxbContext.createUnmarshaller().unmarshal(file);

        domainService.load(domain);
    }

    @Override
    public void xmlSaveJaxb() throws Exception {
        final @NotNull Domain domain = new Domain();
        domainService.export(domain);

        final @NotNull File file = new File(DataConstant.DATA_JAXB_XML_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final @NotNull JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        final @NotNull Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, file);
    }

}