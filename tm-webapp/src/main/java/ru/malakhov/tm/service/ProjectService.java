package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.exception.empty.EmptyDescriptionException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.project.ProjectNotFoundException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.repository.dto.IProjectRepositoryDTO;
import ru.malakhov.tm.repository.entity.IProjectRepository;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IProjectRepositoryDTO projectRepositoryDTO;

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = new Project();
        project.setName(name);
        project.setUser(userService.findById(userId));
        projectRepository.save(project);
    }

    @Override
    public void createDTO(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setUserId(userId);
        projectRepositoryDTO.save(project);
    }

    @Override
    public void create(@Nullable String userId, @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUser(userService.findById("123"));
        projectRepository.save(project);
    }

    @Override
    public void createDTO(@Nullable String userId, @Nullable ProjectDTO project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        projectRepositoryDTO.save(project);
    }

    @Override
    @Transactional
    public void create(final @Nullable String userId, final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(userService.findById(userId));
        projectRepository.save(project);
    }

    @Override
    public @NotNull List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @NotNull List<ProjectDTO> findAllDto() {
        return projectRepositoryDTO.findAll();
    }

    @Override
    public @NotNull List<ProjectDTO> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepositoryDTO.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    public @Nullable Project findById(@Nullable String id) {
        if (id == null && id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public @NotNull Project findOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneByUserIdAndId(userId, id);
    }

    @Override
    public @NotNull ProjectDTO findOneByIdDTO(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepositoryDTO.findOneByUserIdAndId(userId, id);
    }

    @Override
    public @NotNull Project findOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void removeOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        projectRepository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        projectRepository.deleteOneByUserIdAndName(userId, name);
    }

    @Override
    @Transactional
    public void updateProjectById(final @Nullable String userId, final @Nullable String id,
                                  final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Override
    public @NotNull Project findOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final List<Project> projectsList = projectRepository.findAllByUserId(userId);
        if (projectsList == null || projectsList.isEmpty()) throw new ProjectNotFoundException();
        return projectsList.get(index);
    }

    @Override
    @Transactional
    public void removeOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        projectRepository.delete(findOneByIndex(userId, index));
    }

    @Override
    @Transactional
    public void updateProjectByIndex(final @Nullable String userId, final @Nullable Integer index,
                                     final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

}