package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IPropertyService;
import ru.malakhov.tm.api.service.ISessionService;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.repository.dto.ISessionRepositoryDTO;
import ru.malakhov.tm.repository.entity.ISessionRepository;
import ru.malakhov.tm.dto.SessionDTO;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.entity.Session;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.util.SignatureUtil;

import java.util.List;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private ISessionRepository sessionRepository;

    @NotNull
    @Autowired
    private ISessionRepositoryDTO sessionRepositoryDTO;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public boolean checkDataAccess(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        final @NotNull UserDTO userDTO = new UserDTO(userService.findByLogin(login));
        final @Nullable String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(userDTO.getPasswordHash());
    }

    @Override
    @Transactional
    public @Nullable SessionDTO open(final @Nullable String login, final @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull User user = userService.findByLogin(login);
        final @NotNull Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.save(session);
        return sign(new SessionDTO(session));
    }

    @Override
    public @Nullable SessionDTO sign(final @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) return null;
        sessionDTO.setSignature(null);
        final @NotNull String salt = propertyService.getSessionSalt();
        final @NotNull Integer cycle = propertyService.getSessionCycle();
        final @Nullable String signature = SignatureUtil.sign(sessionDTO, salt, cycle);
        sessionDTO.setSignature(signature);
        return sessionDTO;
    }

    @Override
    public @Nullable String getUserId(final @Nullable SessionDTO session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public @NotNull List<SessionDTO> getSessionList(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        return  sessionRepositoryDTO.findAllByUserId(sessionDTO.getUserId());
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        return sessionRepositoryDTO.findAll();
    }

    @Override
    @Transactional
    public void close(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        sessionRepository.deleteOneByUserId(sessionDTO.getUserId());
    }

    @Override
    @Transactional
    public void closeAll(final @Nullable SessionDTO sessionDTO) {
        validate(sessionDTO);
        sessionRepository.deleteAllByUserId(sessionDTO.getUserId());
    }

    @Override
    public boolean isValid(final @Nullable SessionDTO sessionDTO) {
        try {
            validate(sessionDTO);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(final @Nullable SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new  AccessDeniedException();
        if (sessionDTO.getSignature() == null || sessionDTO.getSignature().isEmpty()) throw new  AccessDeniedException();
        if (sessionDTO.getUserId() == null || sessionDTO.getUserId().isEmpty()) throw new  AccessDeniedException();
        if (sessionDTO.getTimestamp() == null) throw new  AccessDeniedException();
        final @Nullable SessionDTO temp = sessionDTO.clone();
        if (temp == null) throw new  AccessDeniedException();
        final @Nullable String signatureSource = sessionDTO.getSignature();
        final @Nullable String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new  AccessDeniedException();
        final boolean isContain = sessionRepository.existsById(sessionDTO.getId());
        if (!isContain) throw new AccessDeniedException();
    }

    @Override
    public void validate(final @Nullable SessionDTO sessionDTO, final @Nullable Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(sessionDTO);
        final @Nullable String userId = sessionDTO.getUserId();
        final @Nullable UserDTO userDTO = new UserDTO(userService.findById(userId));
        if (userDTO.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(userDTO.getRole())) throw new AccessDeniedException();
    }

}