package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.exception.user.AccessDeniedException;
import ru.malakhov.tm.exception.user.UserNotFoundException;
import ru.malakhov.tm.repository.dto.IUserRepositoryDTO;
import ru.malakhov.tm.repository.entity.IUserRepository;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;

import java.util.List;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private IUserRepositoryDTO userRepositoryDTO;

    @Override
    public @NotNull User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    public @NotNull User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @Nullable User user = userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @Override
    @Transactional
    public void removeById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void create(final @Nullable String login, final @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void create(final @Nullable String login, final @Nullable String password, final @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final @NotNull User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public @NotNull List<UserDTO> findAllUsers() {
        return userRepositoryDTO.findAll();
    }

    @Override
    public @NotNull List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public @NotNull UserDTO profile(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        return userRepositoryDTO.findOneById(id);
    }

    @Override
    @Transactional
    public void changeEmail(@Nullable String id, final @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setEmail(email);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changePassword(final @Nullable String id, @Nullable String oldPassword, final @Nullable String newPassword) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new EmptyPasswordException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        if (!user.getPasswordHash().equals(HashUtil.salt(oldPassword))) throw new AccessDeniedException();
        user.setPasswordHash(HashUtil.salt(newPassword));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changeLogin(final @Nullable String id, final @Nullable String login) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLogin(login);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changeFirstName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(name);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changeMiddleName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setMiddleName(name);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changeLastName(final @Nullable String id, final @Nullable String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable User user = userRepository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setLastName(name);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @Nullable User user = userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        final @Nullable User user = userRepository.findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.save(user);
    }

}