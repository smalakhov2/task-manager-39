package ru.malakhov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.enumeration.Status;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.dto.TaskDTO;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @ModelAttribute("projects")
    private List<ProjectDTO> getProjects() {
        return projectService.findAllDto();
    }

    @GetMapping("/task/create")
    public String create() {
        taskService.createDTO("123", "New task");
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.removeOneById("123", id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final @NotNull TaskDTO taskDTO = taskService.findOneByIdDTO("123", id);
        return new ModelAndView("task/task-edit", "task", taskDTO);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute("task")TaskDTO taskDTO, BindingResult result) {
        if (taskDTO.getProjectId().isEmpty()) taskDTO.setProjectId(null);
        taskService.createDTO("123", taskDTO);
        return "redirect:/tasks";
    }

}