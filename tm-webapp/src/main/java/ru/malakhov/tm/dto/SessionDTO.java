package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.Session;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionDTO extends AbstractDTO implements Cloneable {

    public static final long serialVersionUID = 1L;

    @Override
    public SessionDTO clone(){
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e){
            return null;
        }
    }

    @Nullable
    @Column(name = "timestamp")
    private Long timestamp;

    @Nullable
    @Column(name = "user_id")
    private String userId;

    @Nullable
    @Column(name = "signature")
    private String signature;

    public SessionDTO(final @Nullable Session session) {
        if (session == null) return;
        setId(session.getId());
        timestamp = session.getTimestamp();
        userId = session.getUser().getId();
        signature = session.getSignature();
    }

    public @Nullable Long getTimestamp() {
        return timestamp;
    }

    public @Nullable String getUserId() {
        return userId;
    }

    public void setUserId(@Nullable String userId) {
        this.userId = userId;
    }

    public @Nullable String getSignature() {
        return signature;
    }

    public void setSignature(@Nullable String signature) {
        this.signature = signature;
    }

}