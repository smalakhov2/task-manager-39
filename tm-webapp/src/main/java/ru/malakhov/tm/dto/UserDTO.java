package ru.malakhov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractDTO {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "login")
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @Nullable
    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @Column(name = "locked")
    private boolean locked = false;

    public UserDTO(final @NotNull String login, final @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(final @Nullable User user) {
        if (user == null) return;
        setId(user.getId());
        login = user.getLogin();
        passwordHash = user.getPasswordHash();
        email = user.getEmail();
        firstName = user.getFirstName();
        middleName = user.getMiddleName();
        lastName = user.getLastName();
        role = user.getRole();
        locked = user.isLocked();
    }

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLE: " + role;
    }

    public @NotNull String getPasswordHash() {
        return passwordHash;
    }

    public @Nullable Role getRole() {
        return role;
    }

    public void setRole(@Nullable Role role) {
        this.role = role;
    }

}