package ru.malakhov.tm.exception.user;

import ru.malakhov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }
}
