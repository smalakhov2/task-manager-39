package ru.malakhov.tm.exception.empty;

import ru.malakhov.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}