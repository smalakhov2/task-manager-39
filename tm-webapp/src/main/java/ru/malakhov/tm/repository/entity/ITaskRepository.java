package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllByUserIdAndProjectName(@NotNull String userId, @NotNull String projectName);

    void deleteAllByUserId(final @NotNull String userId);

    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    void deleteAllByUserIdAndProjectName(@NotNull String userId, @NotNull String projectName);

    @Nullable
    Task findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findOneByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteOneByUserIdAndName(@NotNull String userId, @NotNull String name);

}