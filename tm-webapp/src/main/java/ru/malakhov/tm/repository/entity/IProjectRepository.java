package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Project;

import java.util.List;

@Repository
public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    List<Project> findAllByUserId(final @NotNull String userId);

    void deleteAllByUserId(final @NotNull String userId);

    @NotNull
    Project findOneByUserIdAndId(final @NotNull String userId, final @NotNull String id);

    @NotNull
    Project findOneByUserIdAndName(final @NotNull String userId, final @NotNull String name);

    void deleteOneByUserIdAndId(final @NotNull String userId, final @NotNull String id);

    void deleteOneByUserIdAndName(final @NotNull String userId, final @NotNull String name);

}