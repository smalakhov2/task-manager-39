package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Session;

import java.util.List;

@Repository
public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    List<Session> findAllByUserId(final @NotNull String userId);

    @NotNull
    List<Session> findAll();

    void deleteOneByUserId(final @NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

}