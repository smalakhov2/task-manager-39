package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.SessionDTO;

import java.util.List;

@Repository
public interface ISessionRepositoryDTO extends IRepositoryDTO<SessionDTO> {

    @Nullable
    List<SessionDTO> findAllByUserId(final @NotNull String userId);

    @NotNull
    List<SessionDTO> findAll();

    void deleteOneByUserId(final @NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

}