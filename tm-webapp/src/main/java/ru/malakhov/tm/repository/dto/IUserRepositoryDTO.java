package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.UserDTO;

import java.util.List;

@Repository
public interface IUserRepositoryDTO extends IRepositoryDTO<UserDTO> {

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(final @NotNull String id);

    @Nullable
    UserDTO findOneByLogin(final @NotNull String login);

    void deleteById(final @NotNull String id);

    void deleteByLogin(final @NotNull String login);

}