package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.User;

import java.util.List;

@Repository
public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(final @NotNull String id);

    @Nullable
    User findOneByLogin(final @NotNull String login);

    void deleteById(final @NotNull String id);

    void deleteByLogin(final @NotNull String login);

}