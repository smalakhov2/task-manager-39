package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.dto.TaskDTO;

import java.util.List;

@Repository
public interface ITaskRepositoryDTO extends IRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    void deleteAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findOneByUserIdAndName(@NotNull String userId, @NotNull String name);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteOneByUserIdAndName(@NotNull String userId, @NotNull String name);

}