package ru.malakhov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity {

    private @Nullable Long timestamp;

    @ManyToOne
    private @Nullable User user;

    private @Nullable String signature;

    public @Nullable Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(@Nullable Long timestamp) {
        this.timestamp = timestamp;
    }

    public @Nullable User getUser() {
        return user;
    }

    public void setUser(@Nullable User user) {
        this.user = user;
    }

    public @Nullable String getSignature() {
        return signature;
    }

}