package ru.malakhov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface SignatureUtil {

    @Nullable
    static String sign(@Nullable Object value, @Nullable String salt, @NotNull Integer cycle) {
        try{
            @NotNull ObjectMapper objectMapper = new ObjectMapper();
            @NotNull String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        }catch (JsonProcessingException e){
            return null;
        }
    }

    static String sign(@Nullable String value, @Nullable String salt, @NotNull Integer cycle) {
        if (value == null || salt == null) return null;
        @Nullable String result = value;
        for (int i = 0; i< cycle; i++){
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }

}