package ru.malakhov.tm.constant;

public interface DataConstant {

    String DATA_BINARY_PATH = "./data.bin";

    String DATA_BASE64_PATH = "./data.base64";

    String DATA_FASTERXML_XML_PATH = "./data_fasterxml.xml";

    String DATA_FASTERXML_JSON_PATH = "./data_fasterxml.json";

    String DATA_JAXB_XML_PATH = "./data_jaxb.xml";

    String DATA_JAXB_JSON_PATH = "./data_jaxb.json";

}