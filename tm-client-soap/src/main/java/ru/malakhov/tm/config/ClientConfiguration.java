package ru.malakhov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.malakhov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.malakhov.tm.endpoint.soap.ProjectSoapEndpointService;
import ru.malakhov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.malakhov.tm.endpoint.soap.TaskSoapEndpointService;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ClientConfiguration {

    @Bean
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    public ProjectSoapEndpoint projectEndpoint(final @NotNull ProjectSoapEndpointService projectEndpointService) {
        return projectEndpointService.getProjectSoapEndpointPort();
    }

    @Bean
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    public TaskSoapEndpoint taskEndpoint(final @NotNull TaskSoapEndpointService taskEndpointService) {
        return taskEndpointService.getTaskSoapEndpointPort();
    }

}