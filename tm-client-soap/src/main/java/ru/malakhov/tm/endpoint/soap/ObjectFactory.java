
package ru.malakhov.tm.endpoint.soap;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.malakhov.tm.endpoint.soap package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateTask_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "createTask");
    private final static QName _CreateTaskResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "createTaskResponse");
    private final static QName _DeleteAllTasks_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "deleteAllTasks");
    private final static QName _DeleteAllTasksResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "deleteAllTasksResponse");
    private final static QName _DeleteOneTaskById_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "deleteOneTaskById");
    private final static QName _DeleteOneTaskByIdResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "deleteOneTaskByIdResponse");
    private final static QName _ExistsTaskById_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "existsTaskById");
    private final static QName _ExistsTaskByIdResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "existsTaskByIdResponse");
    private final static QName _FindOneTaskByIdDTO_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "findOneTaskByIdDTO");
    private final static QName _FindOneTaskByIdDTOResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "findOneTaskByIdDTOResponse");
    private final static QName _GetTasksListDTO_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "getTasksListDTO");
    private final static QName _GetTasksListDTOResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "getTasksListDTOResponse");
    private final static QName _UpdateTask_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "updateTask");
    private final static QName _UpdateTaskResponse_QNAME = new QName("http://soap.endpoint.tm.malakhov.ru/", "updateTaskResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.malakhov.tm.endpoint.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTask }
     * 
     */
    public CreateTask createCreateTask() {
        return new CreateTask();
    }

    /**
     * Create an instance of {@link CreateTaskResponse }
     * 
     */
    public CreateTaskResponse createCreateTaskResponse() {
        return new CreateTaskResponse();
    }

    /**
     * Create an instance of {@link DeleteAllTasks }
     * 
     */
    public DeleteAllTasks createDeleteAllTasks() {
        return new DeleteAllTasks();
    }

    /**
     * Create an instance of {@link DeleteAllTasksResponse }
     * 
     */
    public DeleteAllTasksResponse createDeleteAllTasksResponse() {
        return new DeleteAllTasksResponse();
    }

    /**
     * Create an instance of {@link DeleteOneTaskById }
     * 
     */
    public DeleteOneTaskById createDeleteOneTaskById() {
        return new DeleteOneTaskById();
    }

    /**
     * Create an instance of {@link DeleteOneTaskByIdResponse }
     * 
     */
    public DeleteOneTaskByIdResponse createDeleteOneTaskByIdResponse() {
        return new DeleteOneTaskByIdResponse();
    }

    /**
     * Create an instance of {@link ExistsTaskById }
     * 
     */
    public ExistsTaskById createExistsTaskById() {
        return new ExistsTaskById();
    }

    /**
     * Create an instance of {@link ExistsTaskByIdResponse }
     * 
     */
    public ExistsTaskByIdResponse createExistsTaskByIdResponse() {
        return new ExistsTaskByIdResponse();
    }

    /**
     * Create an instance of {@link FindOneTaskByIdDTO }
     * 
     */
    public FindOneTaskByIdDTO createFindOneTaskByIdDTO() {
        return new FindOneTaskByIdDTO();
    }

    /**
     * Create an instance of {@link FindOneTaskByIdDTOResponse }
     * 
     */
    public FindOneTaskByIdDTOResponse createFindOneTaskByIdDTOResponse() {
        return new FindOneTaskByIdDTOResponse();
    }

    /**
     * Create an instance of {@link GetTasksListDTO }
     * 
     */
    public GetTasksListDTO createGetTasksListDTO() {
        return new GetTasksListDTO();
    }

    /**
     * Create an instance of {@link GetTasksListDTOResponse }
     * 
     */
    public GetTasksListDTOResponse createGetTasksListDTOResponse() {
        return new GetTasksListDTOResponse();
    }

    /**
     * Create an instance of {@link UpdateTask }
     * 
     */
    public UpdateTask createUpdateTask() {
        return new UpdateTask();
    }

    /**
     * Create an instance of {@link UpdateTaskResponse }
     * 
     */
    public UpdateTaskResponse createUpdateTaskResponse() {
        return new UpdateTaskResponse();
    }

    /**
     * Create an instance of {@link TaskDTO }
     * 
     */
    public TaskDTO createTaskDTO() {
        return new TaskDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "createTask")
    public JAXBElement<CreateTask> createCreateTask(CreateTask value) {
        return new JAXBElement<CreateTask>(_CreateTask_QNAME, CreateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "createTaskResponse")
    public JAXBElement<CreateTaskResponse> createCreateTaskResponse(CreateTaskResponse value) {
        return new JAXBElement<CreateTaskResponse>(_CreateTaskResponse_QNAME, CreateTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllTasks }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteAllTasks }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "deleteAllTasks")
    public JAXBElement<DeleteAllTasks> createDeleteAllTasks(DeleteAllTasks value) {
        return new JAXBElement<DeleteAllTasks>(_DeleteAllTasks_QNAME, DeleteAllTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAllTasksResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteAllTasksResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "deleteAllTasksResponse")
    public JAXBElement<DeleteAllTasksResponse> createDeleteAllTasksResponse(DeleteAllTasksResponse value) {
        return new JAXBElement<DeleteAllTasksResponse>(_DeleteAllTasksResponse_QNAME, DeleteAllTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOneTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteOneTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "deleteOneTaskById")
    public JAXBElement<DeleteOneTaskById> createDeleteOneTaskById(DeleteOneTaskById value) {
        return new JAXBElement<DeleteOneTaskById>(_DeleteOneTaskById_QNAME, DeleteOneTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOneTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DeleteOneTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "deleteOneTaskByIdResponse")
    public JAXBElement<DeleteOneTaskByIdResponse> createDeleteOneTaskByIdResponse(DeleteOneTaskByIdResponse value) {
        return new JAXBElement<DeleteOneTaskByIdResponse>(_DeleteOneTaskByIdResponse_QNAME, DeleteOneTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "existsTaskById")
    public JAXBElement<ExistsTaskById> createExistsTaskById(ExistsTaskById value) {
        return new JAXBElement<ExistsTaskById>(_ExistsTaskById_QNAME, ExistsTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistsTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ExistsTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "existsTaskByIdResponse")
    public JAXBElement<ExistsTaskByIdResponse> createExistsTaskByIdResponse(ExistsTaskByIdResponse value) {
        return new JAXBElement<ExistsTaskByIdResponse>(_ExistsTaskByIdResponse_QNAME, ExistsTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIdDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIdDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "findOneTaskByIdDTO")
    public JAXBElement<FindOneTaskByIdDTO> createFindOneTaskByIdDTO(FindOneTaskByIdDTO value) {
        return new JAXBElement<FindOneTaskByIdDTO>(_FindOneTaskByIdDTO_QNAME, FindOneTaskByIdDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIdDTOResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindOneTaskByIdDTOResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "findOneTaskByIdDTOResponse")
    public JAXBElement<FindOneTaskByIdDTOResponse> createFindOneTaskByIdDTOResponse(FindOneTaskByIdDTOResponse value) {
        return new JAXBElement<FindOneTaskByIdDTOResponse>(_FindOneTaskByIdDTOResponse_QNAME, FindOneTaskByIdDTOResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTasksListDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTasksListDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "getTasksListDTO")
    public JAXBElement<GetTasksListDTO> createGetTasksListDTO(GetTasksListDTO value) {
        return new JAXBElement<GetTasksListDTO>(_GetTasksListDTO_QNAME, GetTasksListDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTasksListDTOResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTasksListDTOResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "getTasksListDTOResponse")
    public JAXBElement<GetTasksListDTOResponse> createGetTasksListDTOResponse(GetTasksListDTOResponse value) {
        return new JAXBElement<GetTasksListDTOResponse>(_GetTasksListDTOResponse_QNAME, GetTasksListDTOResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTask }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTask }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "updateTask")
    public JAXBElement<UpdateTask> createUpdateTask(UpdateTask value) {
        return new JAXBElement<UpdateTask>(_UpdateTask_QNAME, UpdateTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateTaskResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateTaskResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://soap.endpoint.tm.malakhov.ru/", name = "updateTaskResponse")
    public JAXBElement<UpdateTaskResponse> createUpdateTaskResponse(UpdateTaskResponse value) {
        return new JAXBElement<UpdateTaskResponse>(_UpdateTaskResponse_QNAME, UpdateTaskResponse.class, null, value);
    }

}
