package ru.malakhov.tm.endpoint.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-12T00:46:36.619+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", name = "TaskSoapEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskSoapEndpoint {

    @WebMethod
    @RequestWrapper(localName = "deleteAllTasks", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.DeleteAllTasks")
    @ResponseWrapper(localName = "deleteAllTasksResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.DeleteAllTasksResponse")
    public void deleteAllTasks()
;

    @WebMethod
    @RequestWrapper(localName = "getTasksListDTO", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.GetTasksListDTO")
    @ResponseWrapper(localName = "getTasksListDTOResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.GetTasksListDTOResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.malakhov.tm.endpoint.soap.TaskDTO> getTasksListDTO()
;

    @WebMethod
    @RequestWrapper(localName = "updateTask", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.UpdateTask")
    @ResponseWrapper(localName = "updateTaskResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.UpdateTaskResponse")
    public void updateTask(

        @WebParam(name = "task", targetNamespace = "")
        ru.malakhov.tm.endpoint.soap.TaskDTO task
    );

    @WebMethod
    @RequestWrapper(localName = "createTask", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.CreateTask")
    @ResponseWrapper(localName = "createTaskResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.CreateTaskResponse")
    public void createTask(

        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @RequestWrapper(localName = "deleteOneTaskById", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.DeleteOneTaskById")
    @ResponseWrapper(localName = "deleteOneTaskByIdResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.DeleteOneTaskByIdResponse")
    public void deleteOneTaskById(

        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @RequestWrapper(localName = "findOneTaskByIdDTO", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.FindOneTaskByIdDTO")
    @ResponseWrapper(localName = "findOneTaskByIdDTOResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.FindOneTaskByIdDTOResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.soap.TaskDTO findOneTaskByIdDTO(

        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @RequestWrapper(localName = "existsTaskById", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.ExistsTaskById")
    @ResponseWrapper(localName = "existsTaskByIdResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.ExistsTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean existsTaskById(

        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );
}
