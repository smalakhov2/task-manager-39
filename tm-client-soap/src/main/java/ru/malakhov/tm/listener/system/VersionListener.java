package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class VersionListener extends AbstractListener {

    @Override
    public @NotNull String arg() {
        return "-v";
    }

    @Override
    public @NotNull String command() {
        return "version";
    }

    @Override
    public @NotNull String description() {
        return "Display program version.";
    }

    @Override
    @EventListener(condition = "@versionListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println("1.3.9");
    }

}
