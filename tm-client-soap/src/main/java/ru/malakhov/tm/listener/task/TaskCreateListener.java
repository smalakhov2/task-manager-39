package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "task-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        taskEndpoint.createTask(name);
        System.out.println("[OK]");
    }

}