package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "project-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new project.";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        projectEndpoint.create(name);
        System.out.println("[OK]");
    }

}