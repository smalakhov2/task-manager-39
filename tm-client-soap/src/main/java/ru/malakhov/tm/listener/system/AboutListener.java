package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @Override
    public @NotNull String arg() {
        return "-a";
    }

    @Override
    public @NotNull String command() {
        return "about";
    }

    @Override
    public @NotNull String description() {
        return "Display developer info.";
    }

    @Override
    @EventListener(condition = "@aboutListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("Name - Sergei Malakhov");
        System.out.println("Email - smalakhov2@rencredit.ru");
    }

}
