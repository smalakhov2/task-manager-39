package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;

@Component
public class TaskDisplayListListener extends AbstractTaskListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "task-list";
    }

    @Override
    public @NotNull String description() {
        return "Display task list.";
    }

    @Override
    @EventListener(condition = "@taskDisplayListListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[TASK LIST]");
        System.out.println(taskEndpoint.getTasksListDTO());
        System.out.println("[OK]");
    }

}